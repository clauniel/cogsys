%Visual Search Task
%(c) Tobias Andersen, 2007

clear
close all
scrsz = get(0,'ScreenSize');
figure('Position',[scrsz(3)/2 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2])
set(gca,'xlim',[-100 100])
set(gca,'ylim',[-100 100])
axis off


[x,y]=meshgrid(-120:10:100,-100:10:100);
N_pos=prod(size(x));
x=reshape(x,1,N_pos);
y=reshape(y,1,N_pos);

Ntrials=20;
DistConds=[6 20 60]; %should be divisible by 2 allowing two distractor types in equal numbers
TargConds=[0 1];

h(1)=text(0,50,'Welcome to visual search!','FontSize',20,'HorizontalAlignment','Center');
h(2)=text(0,30,'Your reaction time is measured,','FontSize',20,'HorizontalAlignment','Center');
h(3)=text(0,10,'So be fast!','FontSize',20,'HorizontalAlignment','Center');
h(4)=text(0,-10,'But also be precise!','FontSize',20,'HorizontalAlignment','Center');
h(5)=text(0,-30,'Press any key to continue','FontSize',20,'HorizontalAlignment','Center');

pause
delete(h), clear h

%%%%% disjunction task %%%%%
h(1)=text(0,50,'Target:','FontSize',20,'HorizontalAlignment','Center');
h(2)=text(0,30,'X','FontSize',20,'color',[0 0 1],'HorizontalAlignment','Center');
h(3)=text(0,10,'or','FontSize',20,'HorizontalAlignment','Center');
h(4)=text(0,-10,'O','FontSize',20,'color',[1 0 0],'HorizontalAlignment','Center');
h(5)=text(0,-40,'Distractor','FontSize',20,'HorizontalAlignment','Center');
h(6)=text(0,-60,'X','FontSize',20,'color',[1 0 0],'HorizontalAlignment','Center');

pause
delete(h), clear h


h(1)=text(0,50,'Press "j" when you see a target','FontSize',20,'HorizontalAlignment','Center');
h(2)=text(0,30,'Press "f" when you see no target','FontSize',20,'HorizontalAlignment','Center');
h(3)=text(0,10,'Press any key to start the experiment','FontSize',20,'HorizontalAlignment','Center');

pause
delete(h), clear h

DistTrial=repmat(DistConds,[1 Ntrials*length(TargConds)]);
DistTrial=DistTrial(randperm(length(DistTrial)));
TargTrial=repmat(TargConds,[1 Ntrials*length(DistConds)]);
TargTrial=TargTrial(randperm(length(DistTrial)));

for trial=1:(Ntrials*length(TargConds)*length(DistConds))

    if round(rand(1)) 
        TargChar='X'; TargCol=[0 0 1];
    else
        TargChar='O'; TargCol=[1 0 0]; 
    end

    ObjPos=randperm(N_pos);
    DistPos=ObjPos(1:DistTrial(trial));
    h1=text(x(DistPos),y(DistPos),'X','FontSize',20,'color',[1 0 0]);

    if TargTrial(trial)
        TargPos=ObjPos(DistTrial(trial)+1);
        h3=text(x(TargPos),y(TargPos),TargChar,'FontSize',20,'color',TargCol);
    end

    tic
    waitforbuttonpress
    RT(trial)=toc;

    delete(h1)
    if TargTrial(trial), delete(h3), clear h3, end

    response=get(1,'currentcharacter');

    if (TargTrial(trial) & response=='j') | (~TargTrial(trial) & response=='f')
        h=text(0,0,'Right!','FontSize',20,'HorizontalAlignment','Center');
        SuccesDisj(trial)=1;
    else
        h=text(0,0,'Wrong!','FontSize',20,'HorizontalAlignment','Center');
        SuccesDisj(trial)=0;
    end
    pause(1)
    delete(h)
    
end

for t=1:length(TargConds)
    for d=1:length(DistConds)
        mRTdisj(t,d)=mean(RT(find(TargTrial==TargConds(t) & DistTrial==DistConds(d))));
        semRTdisj(t,d)=std(RT(find(TargTrial==TargConds(t) & DistTrial==DistConds(d))))/sqrt(Ntrials);
    end
end

%%%%% conjunction task %%%%%
h(1)=text(0,50,'Target:','FontSize',20,'HorizontalAlignment','Center');
h(2)=text(0,30,'X','FontSize',20,'color',[0 0 1],'HorizontalAlignment','Center');
h(3)=text(0,0,'Distractors:','FontSize',20,'HorizontalAlignment','Center');
h(4)=text(0,-20,'O','FontSize',20,'color',[0 0 1],'HorizontalAlignment','Center');
h(5)=text(0,-40,'and','FontSize',20,'HorizontalAlignment','Center');
h(6)=text(0,-60,'X','FontSize',20,'color',[1 0 0],'HorizontalAlignment','Center');

pause
delete(h), clear h

h(1)=text(0,50,'Press "j" when you see a target','FontSize',20,'HorizontalAlignment','Center');
h(2)=text(0,30,'Press "f" when you see no target','FontSize',20,'HorizontalAlignment','Center');
h(3)=text(0,10,'Press any key to start the experiment','FontSize',20,'HorizontalAlignment','Center');

pause
delete(h), clear h

DistTrial=repmat(DistConds,[1 Ntrials*length(TargConds)]);
DistTrial=DistTrial(randperm(length(DistTrial)));
TargTrial=repmat(TargConds,[1 Ntrials*length(DistConds)]);
TargTrial=TargTrial(randperm(length(DistTrial)));


for trial=1:(Ntrials*length(TargConds)*length(DistConds))

    ObjPos=randperm(N_pos);

    DistPos1=ObjPos(1:DistTrial(trial)/2);
    DistPos2=ObjPos((DistTrial(trial)/2+1):DistTrial(trial));
    h1=text(x(DistPos1),y(DistPos1),'X','FontSize',20,'color',[1 0 0]);
    h2=text(x(DistPos2),y(DistPos2),'O','FontSize',20,'color',[0 0 1]);

    if TargTrial(trial)
        TargPos=ObjPos(DistTrial(trial)+1);
        h3=text(x(TargPos),y(TargPos),'X','FontSize',20,'color',[0 0 1]);
    end

    tic
    waitforbuttonpress
    RT(trial)=toc;

    delete(h1), delete(h2)
    if TargTrial(trial), delete(h3), clear h3, end

    response=get(1,'currentcharacter');

    if (TargTrial(trial) & response=='j') | (~TargTrial(trial) & response=='f')
        h=text(0,0,'Right!','FontSize',20,'HorizontalAlignment','Center');
        SuccesConj(trial)=1;
    else
        h=text(0,0,'Wrong!','FontSize',20,'HorizontalAlignment','Center');
        SuccesConj(trial)=0;
    end
    pause(1)
    delete(h)

end

for t=1:length(TargConds)
    for d=1:length(DistConds)
        mRTconj(t,d)=mean(RT(find(TargTrial==TargConds(t) & DistTrial==DistConds(d))));
        semRTconj(t,d)=std(RT(find(TargTrial==TargConds(t) & DistTrial==DistConds(d))))/sqrt(Ntrials);
    end
end

MaxT=max(max(max(ceil(mRTconj))) , max(max(ceil(mRTdisj))));

close
figure(1)
set(1,'name','Feature Disjunction')

subplot(2,1,2)
errorbar(DistConds,mRTdisj(1,:),semRTdisj(1,:),'o')
hold on
plot(DistConds,mRTdisj(1,:))
set(gca,'XTick',DistConds)
set(gca,'YLim',[0 MaxT])
set(gca,'XLim',[0 max(DistConds)+5])
ylabel('Reaction time')
title('Target absent')
xlabel('Number of distractors')

subplot(2,1,1)
errorbar(DistConds,mRTdisj(2,:),semRTdisj(2,:),'o')
hold on
plot(DistConds,mRTdisj(2,:))
set(gca,'XTick',DistConds)
set(gca,'YLim',[0 MaxT])
set(gca,'XLim',[0 max(DistConds)+5])
ylabel('Reaction time')
title('Target present')
xlabel('Number of distractors')

figure(2)
set(2,'name','Feature Conjunction')


subplot(2,1,2)
errorbar(DistConds,mRTconj(1,:),semRTconj(1,:),'o')
hold on
plot(DistConds,mRTconj(1,:))
set(gca,'XTick',DistConds)
set(gca,'YLim',[0 MaxT])
set(gca,'XLim',[0 max(DistConds)+5])
ylabel('Reaction time')
title('Target absent')
xlabel('Number of distractors')

subplot(2,1,1)
errorbar(DistConds,mRTconj(2,:),semRTconj(2,:),'o')
hold on
plot(DistConds,mRTconj(2,:))
set(gca,'XTick',DistConds)
set(gca,'YLim',[0 MaxT])
set(gca,'XLim',[0 max(DistConds)+5])
ylabel('Reaction time')
title('Target present')
xlabel('Number of distractors')



