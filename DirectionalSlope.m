function [ dz ] = DirectionalSlope( pic, ncol, nrow, lightdir )
%DIRECTIONALSLOPE Summary of this function goes here
%   Detailed explanation goes here

dz = zeros(ncol,nrow,8);
epsv = 1.0e-6;
i1 = zeros(1,8);
i2 = zeros(1,8);

for iDir = 1:8
    i1(iDir) = -cos(iDir*0.785398*lightdir(1))-sin(iDir*0.785398*lightdir(2));
    i2(iDir) = sin(iDir*0.785398*lightdir(1)) - cos(iDir*0.785398*lightdir(2));
end
for j = 1:nrow
    for i = 1:ncol
        for iDir = 1:8
            temp = pic(j,i)^2-i2(iDir)^2;
            Det = (1.0 - pic(j,i)^2)*temp;
            dz(j,i,iDir) = -10e10;
            if ( Det >= 0 )
                nom = temp-i1(iDir)^2+1e-10;
                if nom > 0 || i1(iDir)<epsv
                    dz(j,i,iDir) = (-i1(iDir)*lightdir(3)-sqrt(Det))/nom;
                end
            end
            if mod(iDir,2) == 1 
                dz(j,i,iDir) = dz(j,i,iDir)*sqrt(0.5);
            end
        end
    end
end

end

