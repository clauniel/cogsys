clear, close all

%% make shape Z
res = 1e-2; rng = 3;
[X,Y] = meshgrid(-rng:res:rng,-rng:res:rng);
Z = normpdf(X) .* normpdf(Y);
surf(X,Y,Z)
imagesc(Z)

%% illuminate Z to make image I
figure(1)

rho = 1;                %albedo
lambda = 1;             %flux density
 tau = 0;               %tilt
 sigma = pi/2 - pi/36;   %slant


p = normpdf(Y) .* ( -1/sqrt(2*pi) * X .* exp(-X.^2/2));
q = normpdf(X) .* ( -1/sqrt(2*pi) * Y .* exp(-Y.^2/2)); %http://www.wolframalpha.com/input/?i=d%2Fdx+%28a*exp%28-%28x%29%5E2%2F2%29%29

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

I = rho * lambda * (cos(sigma) + k2*p + k3*q);

%surf(X,Y,I)
imagesc(I), colormap gray, axis square

%% Fourer domain version illuminate Z to make image I
figure(2)



Fs = 1e2;

[fx, fy] = meshgrid(-Fs/2:Fs/2/300:Fs/2,-Fs/2:Fs/2/300:Fs/2);

f = sqrt(fx.^2 + fy.^2);
theta = atan2(fy,fx);

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = H .* fftshift(fft2(double(Z)));

I = real(ifft2(ifftshift(Fi)));
%I = medfilt2(I,[21,21]);

imagesc(I), colormap gray, axis square

%axis off;
%set(gcf,'paperunits','centimeters','Paperposition',[0 0 8 4]);
%saveas(gcf, './project/pictures/newbump.eps','psc2');

I2 = I./max(max(I));
%%
derp = imread('project/pictures/sfs.jpg');
derp = double(derp(:,:,1))./255.0;
derp = padarray(derp,[114,51],'replicate','post');
derp = padarray(derp,[114,52],'replicate','pre');
derp = imrotate(derp,0,'bicubic','crop');
I = medfilt2(derp,[21,21]);
%% recover Zhat from I given tau & sigma
figure(3)

taud = 90;
sigmad = 5;

bestk = 0;
minscene = 1e12;

tau = taud*pi/180;
sigma = (90-sigmad)*pi/180;
scene = 0;

%tau = 3*pi/2;               %tilt
%sigma = pi/2 - pi/36;   %slant

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = fftshift(fft2(double(I)));
%imagesc(log(abs(Fi)))
D = Fi./H;[Nx,Ny,Nz] = surfnorm(Z);

R = diffuse(Nx,Ny,Nz,[90,45]);
D(180601) = 0; %ignoring the dc term

Fz_est = real(ifft2(ifftshift(D)));
Fz_est = medfilt2(Fz_est,[21 21]);


figure(2)
imagesc(Fz_est), colormap gray, axis square
[Nx,Ny,Nz] = surfnorm(Fz_est);
R = diffuse(Nx,Ny,Nz,[taud+15,sigmad]);
R = flip(R,2);
R = flip(R,1);
imagesc(R);
axis square; colormap gray;

figure(3)
surfl(Fz_est,[taud,sigmad]); xlim([0 600]); ylim([0 600]); axis off;
shading interp
view(140,40)
%set(gcf,'paperunits','centimeters','Paperposition',[0 0 8 4]);
%saveas(gcf, './project/pictures/sfs270deg.eps','psc2');
xlabel('x')
j=0;
tau = (taud+j)*pi/180;
sigma = (90-sigmad)*pi/180;

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = H .* fftshift(fft2(double(Fz_est)));

I2 = real(ifft2(ifftshift(Fi)));
figure(2)
imagesc(I2)
axis square

scene = scene + sum(sum((I-I2).^2));


%% do it all
figure(3)

taud = 90;
sigmad = 5;

bestk = 0;
minscene = 1e12;

for k = 0:5:355
   taud = k
tau = taud*pi/180;
sigma = (90-sigmad)*pi/180;
scene = 0;

%tau = 3*pi/2;               %tilt
%sigma = pi/2 - pi/36;   %slant

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = fftshift(fft2(double(I)));
%imagesc(log(abs(Fi)))
D = Fi./H;[Nx,Ny,Nz] = surfnorm(Z);

R = diffuse(Nx,Ny,Nz,[90,45]);
D(180601) = 0; %ignoring the dc term

Fz_est = real(ifft2(ifftshift(D)));
Fz_est = medfilt2(Fz_est,[21 21]);


figure(2)
imagesc(Fz_est), colormap gray, axis square
[Nx,Ny,Nz] = surfnorm(Fz_est);
R = diffuse(Nx,Ny,Nz,[taud+15,sigmad]);
R = flip(R,2);
R = flip(R,1);
imagesc(R);
axis square; colormap gray;

figure(3)
surfl(Fz_est,[taud,sigmad]); xlim([0 600]); ylim([0 600]); axis off;
shading interp
view(140,40)
%set(gcf,'paperunits','centimeters','Paperposition',[0 0 8 4]);
%saveas(gcf, './project/pictures/sfs270deg.eps','psc2');
xlabel('x')
for j = -5:1:5
tau = (taud+j)*pi/180;
sigma = (90-sigmad)*pi/180;

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = H .* fftshift(fft2(double(Fz_est)));

I2 = real(ifft2(ifftshift(Fi)));
figure(2)
imagesc(I2)
axis square

scene = scene + sum(sum((I-I2).^2));
end

if scene < minscene
    minscene = scene;
    bestk = k
end
end