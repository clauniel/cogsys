function [ P ] = P1( T, j, tau, t0, D, K, alpha, C )
%P1 Summary of this function goes here
%   Detailed explanation goes here
mu = C/(T+alpha*D);
F = 1-exp(-mu*(tau-t0));
G = 1-exp(-alpha*mu*(tau-t0));
propdist = 0;
for m = 0:min(D,K-j-1)
    propdist = propdist + max(0,nchoosek(D,m)*G^m*(1-G)^(D-m));
end
P = max(nchoosek(T,j)*F^j*(1-F)^(T-j)*propdist,0);
end

