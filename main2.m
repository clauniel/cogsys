clear, close all

%% make shape Z
res = 1e-2; rng = 3;
[X,Y] = meshgrid(-rng:res:rng,-rng:res:rng);
Z = normpdf(X) .* normpdf(Y);
%surf(X,Y,Z)
%imagesc(Z)

%% illuminate Z to make image I
figure(1)

rho = 1;                %albedo
lambda = 1;             %flux density
 tau = pi;               %tilt
 %tau = tau - pi/5

 sigma = pi/2 - pi/36;   %slant


p = normpdf(Y) .* ( -1/sqrt(2*pi) * X .* exp(-X.^2/2));
q = normpdf(X) .* ( -1/sqrt(2*pi) * Y .* exp(-Y.^2/2)); %http://www.wolframalpha.com/input/?i=d%2Fdx+%28a*exp%28-%28x%29%5E2%2F2%29%29

 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

I = rho * lambda * (cos(sigma) + k2*p + k3*q);

%surf(X,Y,I)
imagesc(I), colormap gray, axis square

%% Fourer domain version illuminate Z to make image I
figure(2)

Fs = 1e2;

[fx, fy] = meshgrid(-Fs/2:Fs/2/300:Fs/2,-Fs/2:Fs/2/300:Fs/2);

f = sqrt(fx.^2 + fy.^2);
theta = atan2(fy,fx);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = H .* fftshift(fft2(double(Z)));

I = real(ifft2(ifftshift(Fi)));

imagesc(I), colormap gray, axis square


%% recover Zhat from I given tau & sigma
figure(3)

Fi = fftshift(fft2(double(I)));
%imagesc(log(abs(Fi)))
D = Fi./H;
D(180601) = 0; %ignoring the dc term

Fz_est = real(ifft2(ifftshift(D)));

imagesc(Fz_est), colormap gray, axis square

%% recover Zhat from I given WRONG tau & sigma
figure(4)

 tau = pi;               %tilt
 tau = tau + pi/5;
 
 sigma = pi/2 - pi/36;   %slant
 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));
D = Fi./H;
D(180601) = 0; %ignoring the dc term

Fz_est = real(ifft2(ifftshift(D)));

imagesc(Fz_est), colormap gray, axis square

%% create image of reconstructed shape
 figure(5)
 tau = pi;               %tilt
 tau = tau + pi/5;
 
 sigma = pi/2 - pi/36;   %slant
 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

 H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = H .* fftshift(fft2(double(Fz_est)));

I = real(ifft2(ifftshift(Fi)));
imagesc(I), colormap gray, axis square


%% create image of reconstructed shape w different tau
 figure()
 tau = pi;               %tilt
 %tau = tau + pi/5;
 
 sigma = pi/2 - pi/36;   %slant
 k2 = cos(tau) * sin(sigma);
 k3 = sin(tau) * sin(sigma);

 H = (2*pi*f*exp(pi*i/2) .* (k2*cos(theta) + k3*sin(theta)));

Fi = H .* fftshift(fft2(double(Fz_est)));

I = real(ifft2(ifftshift(Fi)));

imagesc(I), colormap gray, axis square
