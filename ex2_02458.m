d_prime = 1;
lax = 0.1;
moderate = 0.5;
conservative = 0.9;
n_trials = 100000;
rocx = [];
rocy = [];
grocx = [];
grocy = [];
nstim = normrnd(0,1,[n_trials/2,1]);
ystim = normrnd(1.01,1,[n_trials/2,1]);
fpos_lax = sum(nstim>lax)/(n_trials/2);
hits_lax = sum(ystim>lax)/(n_trials/2);
rocx = [rocx,fpos_lax];
rocy = [rocy,hits_lax];
d_calc_lax = norminv(hits_lax,0,1)-norminv(fpos_lax,0,1)
lambda = -norminv(fpos_lax,0,1)
grocx = [grocx,-lambda];
grocy = [grocy,norminv(hits_lax,0,1)];

% moderate
nstim = normrnd(0,1,[n_trials/2,1]);
ystim = normrnd(1.01,1,[n_trials/2,1]);
fpos_moderate = sum(nstim>moderate)/(n_trials/2);
hits_moderate = sum(ystim>moderate)/(n_trials/2);
rocx = [rocx,fpos_moderate];
rocy = [rocy,hits_moderate];
d_calc_moderate = norminv(hits_moderate,0,1)-norminv(fpos_moderate,0,1)
lambda = -norminv(fpos_moderate,0,1)
grocx = [grocx,-lambda];
grocy = [grocy,norminv(hits_moderate,0,1)];

%conservative
nstim = normrnd(0,1,[n_trials/2,1]);
ystim = normrnd(1.01,1,[n_trials/2,1]);
fpos_cons = sum(nstim>conservative)/(n_trials/2);
hits_cons = sum(ystim>conservative)/(n_trials/2);
rocx = [rocx,fpos_cons];
rocy = [rocy,hits_cons];
d_calc_cons = norminv(hits_cons,0,1)-norminv(fpos_cons,0,1)
lambda = -norminv(fpos_cons,0,1)
grocx = [grocx,-lambda];
grocy = [grocy,norminv(hits_cons,0,1)];

figure(1)
plot(rocx,rocy)
xlim([0,1])
ylim([0,1])
figure(2)
plot(grocx,grocy)
axis square
xlim([-1,1])
ylim([-1,1])
[P,S] = polyfit(grocx,grocy,1);
d_reg = P(2)

%% unequal
d_prime = 1;
lax = 0.1;
moderate = 0.5;
conservative = 0.9;
n_trials = 100000;
smu = 2;
ssig = 1.5;
rocx = [];
rocy = [];
grocx = [];
grocy = [];
nstim = normrnd(0,1,[n_trials/2,1]);
ystim = normrnd(smu,ssig,[n_trials/2,1]);
fpos_lax = sum(nstim>lax)/(n_trials/2);
hits_lax = sum(ystim>lax)/(n_trials/2);
rocx = [rocx,fpos_lax];
rocy = [rocy,hits_lax];
d_calc_lax = norminv(hits_lax,0,1)-norminv(fpos_lax,0,1)
lambda = -norminv(fpos_lax,0,1);
grocx = [grocx,-lambda];
grocy = [grocy,norminv(hits_lax,0,1)];

% moderate
nstim = normrnd(0,1,[n_trials/2,1]);
ystim = normrnd(smu,ssig,[n_trials/2,1]);
fpos_moderate = sum(nstim>moderate)/(n_trials/2);
hits_moderate = sum(ystim>moderate)/(n_trials/2);
rocx = [rocx,fpos_moderate];
rocy = [rocy,hits_moderate];
d_calc_moderate = norminv(hits_moderate,0,1)-norminv(fpos_moderate,0,1)
lambda = -norminv(fpos_moderate,0,1);
grocx = [grocx,-lambda];
grocy = [grocy,norminv(hits_moderate,0,1)];

%conservative
nstim = normrnd(0,1,[n_trials/2,1]);
ystim = normrnd(smu,ssig,[n_trials/2,1]);
fpos_cons = sum(nstim>conservative)/(n_trials/2);
hits_cons = sum(ystim>conservative)/(n_trials/2);
rocx = [rocx,fpos_cons];
rocy = [rocy,hits_cons];
d_calc_cons = norminv(hits_cons,0,1)-norminv(fpos_cons,0,1)
lambda = -norminv(fpos_cons,0,1);
grocx = [grocx,-lambda];
grocy = [grocy,norminv(hits_cons,0,1)];

figure(1)
plot(rocx,rocy)
xlim([0,1])
ylim([0,1])
figure(2)
plot(grocx,grocy)
axis square
[P,S] = polyfit(grocx,grocy,1);
ssig_reg = 1/P(1)
smu_reg = ssig_reg*P(2)



