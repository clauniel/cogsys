%% 1
% 2 targets 0 distractors
load('TVAdata2.mat');
num_trials = 60; 
te = data(:,3)'; %expore time vector
cd1ormore = sum(data(:,5:8)')/num_trials; %cumulative chance for 1 or more
cd2ormore = sum(data(:,6:8)')/num_trials; %cumulative chance for 2 or more
figure(1)
plot(te,cd1ormore,'bo-')
hold on
plot(te,cd2ormore,'rx-')
hold off

%want to fit function Tchoosej*F(t)^j*(1-F(t))^(T-j)
%where F=1-exp(mu*(t-t0))
opts = optimset('MaxFunEvals', 2000);
j = 1;
T = 2;
t = data(:,3); %exposure time
d = data(:,5)/60; %chance to perceive 1 at t or before
F = @(x,xdata)(nchoosek(T,j).*(1-exp(-x(1).*(xdata-x(2)))).^j.*(1-(1-exp(-x(1).*(xdata-x(2))))).^(T-j));
x0 = [50, 0]; %arbitrary starting values that converge to a solution
[x,resnorm,~,exitflag,output] = lsqcurvefit(F,x0,t,d,[],[],opts);
x
figure(2)
plot(t,d,'ro')
hold on
plot(t,F(x,t))
hold off
C = x(1)*T
t0 = x(2)

%% 2 targets 0 distractors round 2
% multiple values
load('TVAdata4.mat');
ex1 = data(1:9,:); %data for first curve

% first experiment T = 2, D = 0;
%want to fit function Tchoosej*F(t)^j*(1-F(t))^(T-j)
%where F=1-exp(mu*(t-t0))
opts = optimset('MaxFunEvals', 2000);
j = 1;
T = 2;
t = ex1(:,3); %exposure time
d = ex1(:,5); %chance to perceive 1 at t or before
F = @(x,xdata)(nchoosek(T,j).*(1-exp(-x(1).*(xdata-x(2)))).^j.*(1-(1-exp(-x(1).*(xdata-x(2))))).^(T-j));
x0 = [50, 0]; %arbitrary starting values that converge to a solution
[x,resnorm,~,exitflag,output] = lsqcurvefit(F,x0,t,d,[],[],opts);
x
figure(2)
plot(t,d,'ro')
hold on
plot(t,F(x,t))
hold off
C = x(1)*T
t0 = x(2)

%% derp matlab y u no sum :(
load('TVAdata4.mat');
ex2 = data(10:18,:); % data for second curve

%% 4 targets 0 distractors
load('TVAdata4.mat');
ex3 = data(19:end,:); % data for third curve
% first experiment T = 4, D = 0;
%want to fit function Tchoosej*F(t)^j*(1-F(t))^(T-j)
%where F=1-exp(mu*(t-t0))
opts = optimset('MaxFunEvals', 2000);
j = 1;
T = 4;
t = ex3(:,3); %exposure time
d = ex3(:,5); %chance to perceive 1 at t or before
F = @(x,xdata)(nchoosek(T,j).*(1-exp(-x(1)/T.*(xdata-x(2)))).^j.*(1-(1-exp(-x(1)/T.*(xdata-x(2))))).^(T-j));
x0 = [50, 1]; %arbitrary starting values that converge to a solution
[x,resnorm,~,exitflag,output] = lsqcurvefit(F,x0,t,d,[],[],opts);
x
figure(2)
plot(t,d,'ro')
hold on
plot(t,F(x,t))
hold off
C = x(1)
t0 = x(2)
%% derp
load('TVAdata4.mat');
K=5;
foo = @(x)(TVAerror(x(1), x(2), x(3), K, data))
x0 = [50, 0, 0.1]
[X,FVAL,EXITFLAG] = fminsearch(foo,x0)
%X(1)=80;
%cdxormore( T, D, K, C, t0, alpha, t )
% 2 targets 0 distractors
cd1ormore2t0d = sum(data(1:9,5:8)'); %cumulative chance for 1 or more
cd1ormore2t0dm = cdxormore(2,1,0,K,X(1),X(3),X(3),te)
cd2ormore2t0d = sum(data(1:9,6:8)'); %cumulative chance for 2 or more
cd2ormore2t0dm = cdxormore(2,2,0,K,X(1),X(3),X(3),te)

% 2 targets 2 distractors
cd1ormore2t2d = sum(data(10:18,5:8)'); %cumulative chance for 1 or more
cd1ormore2t2dm = cdxormore(2,1,2,K,X(1),X(3),X(3),te)
cd2ormore2t2d = sum(data(10:18,6:8)'); %cumulative chance for 2 or more
cd2ormore2t2dm = cdxormore(2,2,2,K,X(1),X(3),X(3),te)

%4 targets 0 distractors
cd1ormore4t0d = sum(data(19:end,5:8)'); %cumulative chance for 1 or more
cd1ormore4t0dm = cdxormore(4,1,0,K,X(1),X(3),X(3),te)
cd2ormore4t0d = sum(data(19:end,6:8)'); %cumulative chance for 2 or more
cd2ormore4t0dm = cdxormore(4,2,0,K,X(1),X(3),X(3),te)
cd3ormore4t0d = sum(data(19:end,7:8)'); %cumulative chance for 1 or more
cd3ormore4t0dm = cdxormore(4,3,0,K,X(1),X(3),X(3),te)
cd4ormore4t0d = data(19:end,8:8)'; %cumulative chance for 2 or more
cd4ormore4t0dm = cdxormore(4,4,0,K,X(1),X(3),X(3),te)

te = data(1:9,3)';
figure(1)
plot(te,cd1ormore2t0d,'bo');
hold on
plot(te,cd1ormore2t0dm,'b');
plot(te,cd2ormore2t0d,'gx');
plot(te,cd2ormore2t0dm,'g');
hold off

figure(2)
plot(te,cd1ormore2t0d,'bo');
hold on
plot(te,cd1ormore2t2dm,'b');
plot(te,cd2ormore2t2d,'gx');
plot(te,cd2ormore2t2dm,'g');
hold off

figure(3)
plot(te,cd1ormore4t0d,'bo');
hold on
plot(te,cd1ormore4t0dm,'b');
plot(te,cd2ormore4t0d,'gx');
plot(te,cd2ormore4t0dm,'g');
plot(te,cd3ormore4t0d,'r+');
plot(te,cd3ormore4t0dm,'r');
plot(te,cd4ormore4t0d,'m*');
plot(te,cd4ormore4t0dm,'m');
hold off