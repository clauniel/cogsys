### Spatial Cueing ###
* Repetition priming is not studied in this experiment. I could be studied by testing responses to known figures.
  The expectation priming is a result of the participant knowing that the arrow will point to the target with an 80% chance.
  Endogenous effects are outside effects (top down reasoning) and so when we are told the chance is 80%, we set expectations.
  Repetition responses are exogenous because they are hardcoded learned responses.

* Conjuction: The shape and colour is the intersection between (BLUE & X)
  Disjunction: The shapes and colours are the disjunction ( (BLUE & O) | (RED & X) )
  In the disjunction case, the targets are always different from the distractions.
  In the conjuction case, the target is the same symbol as the distractions.

* Feature binding is only necessary in the conjuction case because you need to react not only on "is it blue" or "is it an O", but "is it blue AND is it an X".

* Little scaling with number of inputs implies heavy parallel processing

* Processing time that scales with input implies serial processing. Feature binding must be serial, but it can't be?

* In the conjuction case your attention spotlight needs to move across the entire range of inputs. In the disjunction case you can view all the inputs at once because no feature binding is needed.

* The slope represents the time per input you need to bind an input. When the feature is absent, this is what shows, because when the feature is present you are likely to find it
  before exhausting the search space.

*  
