%% Load image
figure(1)
mona = imread('MonaLisaBW.jpg');
mona = mona(1:1140,1:end);
colormap('gray');
imagesc(mona)
mona = mat2cell(mona, 10*ones(1,114),10*ones(1,80));
%% do stuff
S = zeros(114*80,100);
for i = 1:114
    for j = 1:80
        foo = mona{i,j}';
        S((i-1)*80+j,:) = foo(1:end);
    end
end
[X,W, E] = pca(S);
recon = W(:,1:6)*X(:,1:6)';

%reconstruct 10x10 matrices
A = []
for i = 1:length(recon)
    A = [A,vec2mat(recon(i,:),10)];
end

Xp = []
for i = 1:length(X)
    Xp = [Xp,vec2mat(X(:,i),10)];
end
%%
figure(2)
colormap('gray')
subplot(2,3,1)
imagesc(Xp(:,1:10))
subplot(2,3,2)
imagesc(Xp(:,1*10+1:1*10+10))
subplot(2,3,3)
imagesc(Xp(:,2*10+1:2*10+10))
subplot(2,3,4)
imagesc(Xp(:,3*10+1:3*10+10))
subplot(2,3,5)
imagesc(Xp(:,4*10+1:4*10+10))
subplot(2,3,6)
imagesc(Xp(:,5*10+1:5*10+10))
%%
B = A(:,1:800);
figure(1)
for i = 1:113
    B = [B;A(:,i*800+1:i*800+800)];
end

imagesc(B)