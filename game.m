%Setup stimulus window
clear
close all
ScrSz = get(0,'ScreenSize');
figure('Position',[1 ScrSz(4) ScrSz(3) ScrSz(4)])
set(gca,'xlim',[-100 100])
set(gca,'ylim',[-100 100])
axis square
axis off
h = text(0,50,'Let''s play a game!','FontSize',20,'HorizontalAlignment','Center');
h(2) = text(0,30,'Guess A or B?','FontSize',20,'HorizontalAlignment','Center');
tic

Nguess=0; Ncorr=0; p = .6;

while toc < 60;

    Nguess = Nguess + 1;
    r='';
    while ~strcmp(upper(r),'A') && ~strcmp(upper(r),'B')
        waitforbuttonpress
        r=get(gcf,'CurrentCharacter');
    end

    if (strcmp(upper(r),'A') && rand<p) || (strcmp(upper(r),'B') && rand<1-p)
        Ncorr = Ncorr +1;
        delete(h);
        h=text(0,50,'Whoo-hoo! You win!!!','FontSize',20,'HorizontalAlignment','Center');
        h(2)=text(0,30,['You''ve guessed right ' num2str(Ncorr) ' times out of ' num2str(Nguess)],'FontSize',20,'HorizontalAlignment','Center');
        pause(1)
        delete(h);
        h(1)=text(0,30,'Let''s play again!','FontSize',20,'HorizontalAlignment','Center');
        h(2) = text(0,10,'Guess A or B?','FontSize',20,'HorizontalAlignment','Center');
    else
        delete(h);
        h=text(0,50,'Boo! You loose!!!','FontSize',20,'HorizontalAlignment','Center');
        h(2)=text(0,30,['You''ve guessed right ' num2str(Ncorr) ' times out of ' num2str(Nguess)],'FontSize',20,'HorizontalAlignment','Center');

        pause(1)
        delete(h);
        h(1)=text(0,30,'Let''s play again!','FontSize',20,'HorizontalAlignment','Center');
        h(2) = text(0,10,'Guess A or B?','FontSize',20,'HorizontalAlignment','Center');
    end

end

delete(h);
h=text(0,50,'Game over!','FontSize',20,'HorizontalAlignment','Center');
h(2)=text(0,30,['You''ve guessed right ' num2str(Ncorr) ' times out of ' num2str(Nguess)],'FontSize',20,'HorizontalAlignment','Center');
pause(2)
h(1)=text(0,10,'Press any key to end','FontSize',20,'HorizontalAlignment','Center');
pause
close all
