function [ height ] = getHeight( iter, dz, ncol, nrow )
%GETHEIGHT Summary of this function goes here
%   Detailed explanation goes here
diS = [1,1;1,0;0,0;0,-1;-1,-1;-1,0;0,0;0,1];
djS = [0,0;0,1;1,1;1,0;0,0;0,-1;-1,-1;-1,0];
height = zeros(nrow,ncol)
epsv = 1e-5
for j2 = 2:nrow
    if mod(iter,4)==0 || mod(iter,4) == 1
        j = j2;
    else
        j = nrow-1-j2;
    end
    for i2 = 2:ncol
        if mod(iter,2) == 1
            i = i2;
        else
            i = ncol-1-i2;
        end
        for iStep = 2:2:8
            dj = djS(iStep,1);
            di = diS(iStep,1);
            z = height(j+djS(iStep+1,1),i+diS(iStep+1,1))+dz(j,i,iStep);
            if z > height(j,i)+epsv
                height(j,i) = z;
            end
            dj = [djS(iStep+1,1),djS(iStep+1,2)] 
            di = [diS(iStep+1,1),diS(iStep+1,2)]
            z = 0.5*(height(j+djS(iStep+1,1),i+diS(iStep+1,1))+height(j+djS(iStep+1,2),i+diS(iStep+1,2)))+dz(j,i,iStep+1);
            if z > height(j,i)+epsv
                height(j,i) = z;
            end
        end
    end
end

end

