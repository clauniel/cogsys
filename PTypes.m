clear, close all

filename = input('Enter a unique numerical identifier for your data filename (e.g. your student ID) ');
if isempty(filename)
    filename = ['PTypeData_' num2str(round(rand*1000))];
else
    filename = ['PTypeData_' num2str(filename)];
end

N_Dots = 3;
N_VLrnPlots=5; N_HVLrnPlots=3;
N_LrnSmpls = N_VLrnPlots * N_HVLrnPlots;
LrnDist = 1.5; TstDists = [LrnDist 1 2 2.5]; N_TstDists = length(TstDists);
N_TstSmpls = N_LrnSmpls;

%PType, Learning Samples and test samples
PType = 2*(rand(1,N_Dots*2)-.5);          %pick a prototype

LrnSmpls = (rand(N_LrnSmpls,N_Dots*2)-.5);
LrnSmpls = LrnDist*LrnSmpls ./ repmat(sqrt(sum(LrnSmpls.^2,2)),[1 N_Dots*2]);
LrnSmpls = LrnSmpls + repmat(PType,[N_LrnSmpls 1]);

LrnSmpls2 = (rand(N_LrnSmpls,N_Dots*2)-.5);
LrnSmpls2 = TstDists(end)*LrnSmpls2 ./ repmat(sqrt(sum(LrnSmpls2.^2,2)),[1 N_Dots*2]);
LrnSmpls2 = LrnSmpls2 + repmat(PType,[N_LrnSmpls 1]);

TstSmpls = (rand(N_TstDists,N_TstSmpls,N_Dots*2)-.5);
TstSmpls = TstSmpls ./ repmat(sqrt(sum(TstSmpls.^2,3)),[1 1 N_Dots*2]);
TstSmpls = TstSmpls .* repmat(TstDists',[1 N_TstSmpls,2*N_Dots]);
TstSmpls = TstSmpls + repmat(reshape(PType,[1 size(PType)]),[N_TstDists N_TstSmpls 1]);

Smpls(1,:,:)=LrnSmpls; Smpls(2,:,:)=LrnSmpls2; Smpls(3:(2+N_TstDists),:,:)=TstSmpls;

%Setup stimulus window
close all
ScrSz = get(0,'ScreenSize');
figure('Position',[1 ScrSz(4) ScrSz(3) ScrSz(4)])
ColorMtrx = [eye(3); 0 0 0; 1 1 1;1 1 0;1 0 1;0 1 1];

%plot learning display 1
XAXlim = [min(min(min(Smpls(:,:,1:2:(2*N_Dots-1))))) max(max(max(Smpls(:,:,1:2:(2*N_Dots-1)))))] + .2*[-1 1];
YAXlim = [min(min(min(Smpls(:,:,2:2:(2*N_Dots))))) max(max(max(Smpls(:,:,2:2:(2*N_Dots)))))] + .2*[-1 1];
for p=1:N_LrnSmpls
    subplot(N_HVLrnPlots,N_VLrnPlots,p)
    hold on

    set(gca,'xlim',XAXlim);
    set(gca,'ylim',YAXlim);
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    box on

    for d=1:N_Dots
        plot(LrnSmpls(p,2*d-1),LrnSmpls(p,d*2),'o','MarkerFaceColor',ColorMtrx(d,:),'MarkerSize',15);
    end
end

subplot(N_HVLrnPlots,N_VLrnPlots,3)
h=title('Look at all these Leptons!');
set(h,'color',[1 0 0])
set(h,'FontSize',16)
%drawnow
waitforbuttonpress


%plot learning display 2
close, figure('Position',[1 ScrSz(4) ScrSz(3) ScrSz(4)])
for p=1:N_LrnSmpls
    subplot(N_HVLrnPlots,N_VLrnPlots,p)
    hold on

    set(gca,'xlim',XAXlim);
    set(gca,'ylim',YAXlim);
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    box on

    for d=1:N_Dots
        plot(LrnSmpls2(p,2*d-1),LrnSmpls2(p,d*2),'o','MarkerFaceColor',ColorMtrx(d,:),'MarkerSize',15);
    end
end
subplot(N_HVLrnPlots,N_VLrnPlots,3)
h=title('These are totally not Leptons!');
set(h,'color',[1 0 0])
set(h,'FontSize',16)
%drawnow
waitforbuttonpress


subplot(N_HVLrnPlots,N_VLrnPlots,3)
h=title('These are not Leptons!');
set(h,'color',[1 0 0])
set(h,'FontSize',16)

%start test
close, figure('Position',[1 ScrSz(4) ScrSz(3) ScrSz(4)])
N_TrialTypes = size(Smpls,1);
N_TrialsTotal = N_TrialTypes * N_LrnSmpls;
TrialType = repmat(1:N_TrialTypes,[N_LrnSmpls 1]); TrialType = TrialType(randperm(N_TrialsTotal));
N_TrialsShown = zeros(1,N_TrialTypes);
Resp = zeros(1,N_TrialTypes);
for t=1:N_TrialsTotal
    subplot(N_HVLrnPlots,N_VLrnPlots,8)
    set(gca,'xlim',XAXlim);
    set(gca,'ylim',YAXlim);
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    box on

    for d=1:N_Dots
        plot(Smpls(TrialType(t),N_TrialsShown(TrialType(t))+1,2*d-1),Smpls(TrialType(t),N_TrialsShown(TrialType(t))+1,d*2),'o','MarkerFaceColor',ColorMtrx(d,:),'MarkerSize',15);
        hold on
    end
    set(gca,'xlim',XAXlim);
    set(gca,'ylim',YAXlim);
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    box on
    h=title('Is this a Lepton? (Y/N)');
    set(h,'FontSize',16)

    N_TrialsShown(TrialType(t)) = N_TrialsShown(TrialType(t)) + 1;
    hold off

    r='';
    while ~strcmp(upper(r),'Y') && ~strcmp(upper(r),'N')
        waitforbuttonpress
        r=get(gcf,'CurrentCharacter');
    end

    Resp(TrialType(t)) = Resp(TrialType(t))+strcmp(upper(r),'Y');

end

%plot data
close all
data(1,1:2) = Resp([1 3]); 
data(2,1:2) = [Resp(4) 0]; 
data(3,1:2) = [Resp(5) 0]; 
data(4,1:2) = Resp([6 2]);
data = 100 * data / N_LrnSmpls;

subplot(2,1,1)
bar(TstDists,data(:,1))
ylabel('%')
xlabel('Distance from Prototype')
legend('Test')
set(gca,'Ylim',[0 100])
set(gca,'Xtick',sort(TstDists))
hold on
errorbar(TstDists,data(:,1),sqrt(data(:,1).*(100-data(:,1))/15),'ko')


subplot(2,1,2)
bar(TstDists,data(:,2))
ylabel('%')
xlabel('Distance from Prototype')
legend('Learning')
set(gca,'Ylim',[0 100])
set(gca,'Xtick',TstDists([1 N_TstDists]))
hold on
errorbar(TstDists(1),data(1,2),sqrt(data(1,2).*(100-data(1,2))/15),'ko')
errorbar(TstDists(4),data(4,2),sqrt(data(4,2).*(100-data(4,2))/15),'ko')


save(filename,'Resp')
