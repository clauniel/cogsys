%% generate gaussian bump
x = linspace(-5,5,128);
y = linspace(-5,5,128);
[X Y] = meshgrid(x,y);
Z = mvnpdf([X(:) Y(:)]);
Z = reshape(Z,size(X));
%surf(X,Y,Z,gradient(Z));
%surfl(X,Y,Z,[5,5]);
[Nx,Ny,Nz] = surfnorm(Z);
R = diffuse(Nx,Ny,Nz,[90,45]);
figure(1)
imagesc(R);
colormap gray;
shading interp
view(0,90);
set(gca,'Visible','off')
set(gca,'position',[0 0 1 1], 'units', 'normalized')
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 1.28 1.28])
print -djpeg gbump.jpg -r100;
%%
size = 128;
Zn1 = zeros(size,size);
Zn = zeros(size, size);
Si = zeros(size,size);
Si1 = ones(size,size)*0.01;
iter = 2;
pic = double(imread('gbump.jpg'))/255.0;
Ps = 1*pi/180;
Qs = 4*pi/180;
Wn = 0.0001^2;


for I = 1:iter
    for i = 1:size
        for j = 1:size
            if j - 1 == 0 || i - 1 == 0
                p = 0;
                q = 0;
            else
                p = Zn1(i,j) - Zn1(i,j-1);
                q = Zn1(i,j) - Zn(i-1,j);
            end
            pq = 1 + p^2+q^2;
            PQs = 1 + Ps^2+Qs^2;
            Eij  = pic(i,j);
            fZ = -1*(Eij - max(0,(1+p*Ps+q*Qs)/(sqrt(pq)*sqrt(PQs))));
            dfZ = -1*((Ps+Qs)/(sqrt(pq)*sqrt(PQs))-(p+q)*(1+p*Ps+q*Qs)/(sqrt(pq^3)*sqrt(PQs)));
            Y = fZ + dfZ*Zn1(i,j);
            K = Si1(i,j)*dfZ/(Wn + dfZ*Si1(i,j)*dfZ);
            Si(i,j) = (1-K*dfZ)*Si1(i,j);
            Zn(i,j) = Zn1(i,j) + K*(Y-dfZ*Zn1(i,j));
        end
    end
    Zn1 = Zn;
    Si1 = Si;
end
surfl(Zn,[270,5])     
shading interp
%%
derp = double(imread('gbump.jpg'))/255;
slant = 80*pi/180;
tilt = 85*pi/180;
rf = 10;
theta = 10;

derp = derp(:,:,1);
fftderp = fft2(derp);
%fftderp(1,1) = 0;
fftderp = fftshift(fftderp);
fftderp(65,65)=0;
k2 = cos(tilt)*sin(slant);
k3 = sin(slant)*sin(tilt);

impf = zeros(128,128);

for k = 1:128
    for j = 1:128
        if ( ( k ~= 65 ) && ( j ~= 65 ) )
            xi = (k-65)/128;
            yi = (j-65)/128;
            mag = abs(xi+yi*i);
            ang = angle(xi+yi*i);
            impf(k,j) = (2*pi*mag*exp(i*pi/2)*(k2*cos(ang)+k3*sin(ang)))^(-1)*fftderp(k,j);
            
        end
    end
end

impf=ifftshift(impf);

Z = abs(ifft2(impf,'nonsymmetric'));
figure(2)
%Z = medfilt2(Z,[21,21]);
surfl(X,Y,Z,[90,0])
view(0,90);
shading interp;
colormap gray(256);
lighting none;

%Z = abs(ifft2((2*pi*rf*exp(-i*pi/2)*(k2*cos(theta)+k3*sin(theta)))^(-1)*fft2(imp)));
%Z = Z(:,:,1);
%surf(Z)
%%

image = double(imread('gbump.jpg'))/255;
% find singular points
maxval = max(max(image));
index = find(image==maxval(1));
index = index(1);

%image size
size = 128;

%light direction in image coordinates
light = [1,0,-0.1];
light = light/norm(light);

Z = zeros(size,size);
Ztheta = zeros(size,size);

%%
image = double(imread('gbump.jpg'))/255;
%image size
size = 128;

%light direction in image coordinates
light = [1,0,-0.1];
light = light/norm(light);
dz = DirectionalSlope(image,size,size,light);
getHeight(4, dz, size, size);
max(dz)

%%
x = linspace(-5,5,128);
y = linspace(-5,5,128);
[X Y] = meshgrid(x,y);
Z = mvnpdf([X(:) Y(:)]);
Z = reshape(Z,size(X));
surf(X,Y,Z);

%%
M=128;
N=128;
slant=120*pi/180;
tilt=45*pi/180;
% read the image
E = imread('gbump.jpg');
% making sure that it is a grayscale image
E = double(E(:,:,1));
% normalizing the image to have maximum of one
E = E ./ max(E(:));
% first compute the surface albedo and illuminationdirection ...
%[albedo,I,slant,tilt] = estimate_albedo_illumination (E);

% compute the fourier transform of the image
Fe = fft2(E); Fe(1,1)=0;

% wx and wy in (47) and (48)
[M,N] = size(E);
[x,y] = meshgrid(1:N,1:M);
wx = (2.* pi .* x) ./ M;
wy = (2.* pi .* y) ./ N;

% Using the estimated illumination direction, compute (55).
Fz = Fe./((-i.*wx.*cos(tilt).*sin(slant)-i.*wy.*sin(tilt).*sin(slant)));
Fe(1,1)=0;

% Compute the inverse Fourier transform to recover the surface.
IZ = ifft2(Fz);
Zmag = abs(IZ);
Zp = angle(IZ);
Z = Zmag.*(cos(Zp)+sin(Zp));
%Z = abs(IZ);
[Nx,Ny,Nz] = surfnorm(Z);
R = diffuse(Nx,Ny,Nz,[120,45]);
figure(2)
imagesc(R);
Z = medfilt2(Z,[21,21]);
% visualizing the result

%h = surfl(X,Y,Z,[90,25]);
shading interp;
colormap gray(256);
view(0,90)

%%
% read the image
E = imread('~/Downloads/sphere/real1.bmp');
% making sure that it is a grayscale image
E = double(E(:,:,1));

slant = 45*pi/180;
tilt = 15*pi/180;

% initializations ...
[M,N] = size(E);
% surface normals
p = zeros(M,N);
q = zeros(M,N);
% the surface
Z = zeros(M,N);
% surface derivatives in x and y directions
Z_x = zeros(M,N);
Z_y = zeros(M,N);
% maximum number of iterations
maxIter = 200;
% the normalized illumination direction
ix = cos(tilt) * tan(slant);
iy = sin(tilt) * tan(slant);

for k = 1 : maxIter
% using the illumination direction and the currently estimate
% surface normals, compute the corresponding reflectance map.
% refer to (57) ...
R =(cos(slant) + p .* cos(tilt)*sin(slant)+ q .*sin(tilt)*sin(slant))./sqrt(1 + p.^2 + q.^2);
% at each iteration, make sure that the reflectance map is positive at
% each pixel, set negative values to zero.
R = max(0,R);
% compute our function f which is the deviation of the computed
% reflectance map from the original image ...
f = E-R;
% compute the derivative of f with respect to our surface Z ... refer
% to (62)
df_dZ = (p+q).*(ix*p + iy*q + 1)./(sqrt((1 + p.^2 + q.^2).^3)*sqrt(1 + ix^2 + iy^2))-(ix+iy)./(sqrt(1 + p.^2 + q.^2)*sqrt(1 + ix^2+ iy^2));
% update our surface ... refer to (61)
Z = Z-f./(df_dZ + eps);
% to avoid dividing by zero
% compute the surface derivatives with respect to x and y
Z_x(2:M,:) = Z(1:M-1,:);
Z_y(:,2:N) = Z(:,1:N-1);
% using the updated surface, compute new surface normals, refer to (58)
% and (59)
p = Z-Z_x;
q = Z-Z_y;
end

% smoothing the recovered surface
Z = medfilt2(abs(Z),[21 21]);
% visualizing the result
figure(2);
surfl(Z);
shading interp;
colormap gray(256);