%jujus
ripe_juju_p = 0.15; %ripe juju chance
uripe_juju_p = 1-ripe_juju_p;
ripe_juju_c = 600; %mean 600nm
ripe_juju_sd = 50; %sd 50nm
uripe_juju_c = 500;
uripe_juju_sd = 50;
juju_p = 0.1; %overall juju chance

%mongo berries
ripe_mongo_p = 0.8;
uripe_mongo_p = 1-ripe_mongo_p;
ripe_mongo_c = 580;
ripe_mongo_sd = 20;
uripe_mongo_c = 520;
uripe_mongo_sd = 20;
mongo_p = 0.5;

%chakavas
ripe_chak_p = 0.1;
uripe_chak_p = 1-ripe_chak_p;
ripe_chak_c = 400;
ripe_chak_sd = 100;
uripe_chak_c = 550;
uripe_chak_sd = 100;
chak_p = 0.4;

%juju between 540 and 550 ripe chance
ijuju_r = cdf('norm',550,ripe_juju_c,ripe_juju_sd)-cdf('norm',540,ripe_juju_c,ripe_juju_sd);
ijuju_u = cdf('norm',550,uripe_juju_c,uripe_juju_sd)-cdf('norm',540,uripe_juju_c,uripe_juju_sd);
ripe_juju = ijuju_r*ripe_juju_p/(ripe_juju_p*ijuju_r+uripe_juju_p*ijuju_u)

%random fruit ripe chance between 540 and 550
imongo_r = cdf('norm',550,ripe_mongo_c,ripe_mongo_sd)-cdf('norm',540,ripe_mongo_c,ripe_mongo_sd);
imongo_u = cdf('norm',550,uripe_mongo_c,uripe_mongo_sd)-cdf('norm',540,uripe_mongo_c,uripe_mongo_sd);
ichak_r = cdf('norm',550,ripe_chak_c,ripe_chak_sd)-cdf('norm',540,ripe_chak_c,ripe_chak_sd);
ichak_u = cdf('norm',550,uripe_chak_c,uripe_chak_sd)-cdf('norm',540,uripe_chak_c,uripe_chak_sd);

ripe_random=...
(ijuju_r*ripe_juju_p*juju_p+imongo_r*ripe_mongo_p*mongo_p+ichak_r*ripe_chak_p*chak_p)/((ripe_juju_p*ijuju_r+uripe_juju_p*ijuju_u)*juju_p+(ripe_mongo_p*imongo_r+uripe_mongo_p*imongo_u)*mongo_p+(ripe_chak_p*ichak_r+uripe_chak_p*ichak_u)*chak_p)

%generate N random numbers
N_fruits = 1000;
rnums = rand(1,N_fruits);
%assign fruit names
fruits = zeros(1,N_fruits);
ripe_juju = 1;
uripe_juju = 2;
ripe_mongo = 3;
uripe_mongo = 4;
ripe_chak = 5;
uripe_chak = 5;
for fruit = 1:length(rnums)
    if rnums(fruit) <= juju_p
        if rand(1) <= ripe_juju_p
            fruits(fruit) = ripe_juju;
        else
            fruits(fruit) = uripe_juju;
        end
    elseif rnums(fruit) <= mongo_p+juju_p
        if rand(1) <= ripe_mongo_p
            fruits(fruit) = ripe_mongo;
        else
            fruits(fruit) = uripe_mongo;
        end
    else
        if rand(1) <= ripe_chak_p
            fruits(fruit) = ripe_chak;
        else
            fruits(fruit) = uripe_chak;
        end
    end
end

%generate color
colors = zeros(1,N_fruits);
for fruit = 1:length(fruits)
    if fruits(fruit) == ripe_juju
        colors(fruit) = normrnd(ripe_juju_c,ripe_juju_sd);
    elseif fruits(fruit) == uripe_juju
        colors(fruit) = normrnd(uripe_juju_c,uripe_juju_sd);
    elseif fruits(fruit) == ripe_mongo
        colors(fruit) = normrnd(ripe_mongo_c,ripe_mongo_sd);
    elseif fruits(fruit) == uripe_mongo
        colors(fruit) = normrnd(uripe_mongo_c,uripe_mongo_sd);
    elseif fruits(fruit) == ripe_chak
        colors(fruit) = normrnd(ripe_chak_c,ripe_chak_sd);
    elseif fruits(fruit) == uripe_chak
        colors(fruit) = normrnd(uripe_chak_c,uripe_chak_sd);
    end
end

%round to nearest 10 to get +-5 resolution
colors = round(colors/10)*10;

num_ripe_picked = 0;
num_uripe_picked = 0;
for fruit = 1:length(colors)
    %random fruit ripe given color seen by monkey
    imongo_r = cdf('norm',colors(fruit)+5,ripe_mongo_c,ripe_mongo_sd)-cdf('norm',colors(fruit)-5,ripe_mongo_c,ripe_mongo_sd);
    imongo_u = cdf('norm',colors(fruit)+5,uripe_mongo_c,uripe_mongo_sd)-cdf('norm',colors(fruit)-5,uripe_mongo_c,uripe_mongo_sd);
    ichak_r = cdf('norm',colors(fruit)+5,ripe_chak_c,ripe_chak_sd)-cdf('norm',colors(fruit)-5,ripe_chak_c,ripe_chak_sd);
    ichak_u = cdf('norm',colors(fruit)+5,uripe_chak_c,uripe_chak_sd)-cdf('norm',colors(fruit)-5,uripe_chak_c,uripe_chak_sd);
    
    fruit_type = fruits(fruit);
    
    ripe_random=...
    (ijuju_r*ripe_juju_p*juju_p+imongo_r*ripe_mongo_p*mongo_p+ichak_r*ripe_chak_p*chak_p)/((ripe_juju_p*ijuju_r+uripe_juju_p*ijuju_u)*juju_p+(ripe_mongo_p*imongo_r+uripe_mongo_p*imongo_u)*mongo_p+(ripe_chak_p*ichak_r+uripe_chak_p*ichak_u)*chak_p);
    
    if ripe_random > 0.5
        if fruit_type == ripe_juju || fruit_type == ripe_mongo || fruit_type == ripe_chak
            num_ripe_picked = num_ripe_picked + 1;
        else
            num_uripe_picked = num_uripe_picked +1;
        end
    end
end

efficiency = num_ripe_picked/(num_uripe_picked+num_ripe_picked)
        
        