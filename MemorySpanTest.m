clear
close all

P1=1;

Cons=upper('bcdfghjklmnpqrstvxz');
N_Cons=length(Cons);
N_items=5:8;
N_trials_pr_item=10;
N_items_this_trial=[];
N_items_this_trial = repmat(N_items,[1 N_trials_pr_item]);
N_items_this_trial = N_items_this_trial(randperm(length(N_items_this_trial)));
N_trials=N_trials_pr_item*length(N_items);
N_items_this_trial=N_items_this_trial(randperm(N_trials));

figure(1)
set(gca,'xlim',[-100 100])
set(gca,'ylim',[-100 100])
axis square
axis off

for t=1:N_trials


    %stimulus presentation
    figure(1)
    h=text(0,0,'!','FontSize',60);
    pause(1)
    delete(h)
    pause(1)
    Cons=Cons(randperm(N_Cons));

    for c=1:N_items_this_trial(t)
        figure(1)
        h=text(0,0,Cons(c),'FontSize',60);
        pause(P1)
        delete(h)
    end

    answer=input('Type the text you saw, in correct order, here: ','s');

    answer=upper(answer);

    N_correct(t)=0;
    for n=1:min(length(answer),N_items_this_trial(t))
        N_correct(t)=N_correct(t)+(answer(n)==Cons(n));
    end
end

for n=1:length(N_items)
    NumberCorrect(n,:)=(N_correct(find(N_items_this_trial==N_items(n))));
end
NumberCorrect=NumberCorrect'
close
save NumberCorrect NumberCorrect, clear, load NumberCorrect
