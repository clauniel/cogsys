\include{pre}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\begin{document}
\begin{titlepage}
	\begin{center}

		% Upper part of the page. The '~' is needed because \\
		% only works if a paragraph has started.

		\textsc{\LARGE }\\[1.5cm]

		\textsc{\Large }\\[0.5cm]

		% Title
		\HRule \\[0.4cm]
		{\huge \bfseries Bayesian Shape from Shading \\[0.4cm] }
		02458 Cognitive Modelling E14 \\
		Carsten Nielsen, s123161

		\HRule \\[1.5cm]

		\vfill

		% Bottom of the page
		{\large \today}

	\end{center}
\end{titlepage}
%\section*{Project Proposal: Inferring shape and viewpoint from a shaded image}
%The aim of this project is to develop a Bayesian model of perception is consistent with the human perception of the Necker Cube.
%The model should also be consistent with other types of shapes.
%
%Development of the model will begin with replication of the work done by Freeman on inferring 3d shape and lighting
%direction from shaded 2d images\cite{Freeman1996}. Here, the viewpoint is inferred by finding the 3d
%shape that will generate the given 2d image while being as insensitive to the azimuth angle of the lighting as possible.
%In effect, the direction of lighting has been discounted.
%
%This work should then be extended to cover the Necker Cube. This problem does not have the problem of directional lighting,
%instead the viewpoint must be inferred. It is hoped that the same technique use for the shaded 2d images can be applied to the Necker Cube.
%Different priors, perhaps a preference for specific angles, can then be added to extend the model.
%
%If modelling the perception of the Necker Cube is succesful, the model should be tested on other shapes and adjustments should
%be made as appropriate.
%
%The aim of this project is to replicate the work done by Freeman on inferring 3d shape and lighting direction from 
%shaded 2d images\cite{Freeman1996}. In Freeman's model, the viewpoint is inferred by finding the 3d shape that will generate the 
%given 2d image while being as insensitive to the azimuth angle of the lighting as possible.
%In effect, the direction of lighting has been discounted and the perceived 3d shape is the shape that was used to 
%discount the lighting direction.
\section{Introduction}
Human perception is able to infer three-dimensional shape from two-dimensional images. One example of this
ability is the Necker cube, a wireframe figure that is interpreted as either three-dimensional cube seen
from below or from above. With training it is possible to switch between interpretations, the figure is
said to be bistable. While there are an infinite number of three-dimensional wireframe configurations that will create
the image of the Necker cube if projected onto a plane, human perception is limited to the two possibilites
shown in~\ref{fig:Necker} as commented on by Kersten and Yuille~\cite{Kersten2003} as well as 
Mamassian, Landy and Maloney~\cite{Mamassian2002}. 
%TODO: sometthing about priors
\begin{figure}[h]
	\center
	\begin{subfigure}[b]{0.3\textwidth}
		\center
		\includegraphics[height=2.7cm]{./pictures/Necker_cube}
		\caption{}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/Cube1}
		\caption{}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/Cube2}
		\caption{}
	\end{subfigure}
	\caption{The Necker cube (a) and the two possible interpretations, a cube seen from above (b) and below (c)\protect\footnotemark}
	\label{fig:Necker}
\end{figure}
\footnotetext{Reproduced from \url{https://en.wikipedia.org/wiki/Necker_cube}}
Mamassian et al.\ developed a bayesian model of visual perception explaining the perceived three-dimensional structure of both
two-dimensional shaded images and surface projections as a function of view angle. 
For the shaded images, they found a strong prior belief for light sources
being located in the upper left of the viewed image. For surfaces, they discovered a prior assumption of surface normals generally 
pointing upwards and inferred that this affects the perception of the Necker cube.

The assumption of the light source illuminating an image being placed in the upper left of the image itself can be seen 
in figure~\ref{fig:uplprior}. The two images seem different because the shading causes a perception of a three-dimensional 
structure. In truth the images are the same, only rotated 180 degrees.

The shape from shading problem, originally solved by Horn (see~\cite{Horn1990} for his review), deals with obtaining a three-dimensional
shape from two-dimensional shaded images. In order to do this, surface lighting conditions as well as surface reflectance must be
estimated, and while methods exist for estimating these parameters they do not work well on real world data. 
When the lighting parameters are known and the surface reflection function can be approximated as being lambertian, meaning
reflectance depends only on the angle between the lighting vector and surface normal, Pentland has shown that his linear shape
from shading algorithm is capable of explaining any image~\cite{Pentland1990}. When the lighting direction is not known, an 
infinite number of surface geometries can explain any given image. Freeman proposed solving this problem by invoking 
"the generic viewpoint assumption"~\cite{Freeman1996}, a bayesian shape from shading algorithm that evaluates hypothesis shapes
generated by Pentland's algorithm. The hypothesis shapes are evaluated according to the scene probability equation which 
estimates the likehood of a hypothesis shape being correct based on how much the observed image would change for a small
change in lighting direction. Freeman speculated that this method is similar to the one used in the human visual perception system
and proposed that a shape from shading algorithm based on the scene probability equation could be implemented.

The aim of this report is replicate the scene probability evaluation described by Freeman, and extend it to work as
part of a full shape from shading algorithm. I will be applying it to the same stimuli as Freeman in order to show 
compatibility, as well as to the shaded image shown in figure~\ref{fig:uplprior} to verify that it produces the
same three-dimensional surface as perceived by a human observer.
\begin{figure}[h]
	\center
	\begin{subfigure}[b]{0.3\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/sfs.jpg}
		\caption{}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\center
		\includegraphics[height=3cm,angle=180]{./pictures/sfs.jpg}
		\caption{}
	\end{subfigure}
	\caption{Two images containing shaded images\protect\footnotemark. The visual perception indicates a dimpled surface where (a) and (b) have
		distinctly different configurations in terms of what dimples are convex and concave. This is a result of the prior assumption of lighting
	direction. In truth the images are the same, only rotated 180 degrees.}
	\label{fig:uplprior}
\end{figure}
\footnotetext{Reproduced from \url{http://www.psychol.ucl.ac.uk/vision/Lab_Site/Demos.html}}

\section{The bayesian framework for visual perception and linear shape from shading}
\subsection{The bayesian frame for visual perception}
The images presented in the introduction are examples of how the visual system prefers certain interpretations over others.
This perception of specific shapes can be modelled as a preference by the visual system for interpretations that maximise the
likelihood of the perceived shape being real. Bayes' rule, which provides a starting point for this model, states that 
the posterior probability (likelihood) of \(A\), given \(B\), is
\begin{equation*}
	P(A|B) = \frac{P(B|A)P(A)}{P(B)}
\end{equation*}
Where \(P(A)\) is the prior assumption or belief in \(A\) and \(P(B)\) is the likelihood of \(B\) occuring regardless of \(A\).

We can apply Bayes' rule to the probability of observing a specific three-dimensional scene \(S\), when presented with an
image \(I\). In this case Bayes' rule becomes
\begin{equation*}
	P(S|I) = \frac{P(I|S)P(S)}{P(I)}
\end{equation*}
Where \(P(S)\) is the prior preference for or belief in that \(S\) should be observed regardless of the image being presented.
To arrive at the same result as the visual system, the scene maximising the posterior probability is chosen.

For any real scene, the observed image is a function of several parameters. These include the reflectance of surfaces in the scene,
the number and direction of light sources and the position of objects relative to each other.
Kersten and Yuille describe discounting as the process of eliminating these parameters by integrating them out. 
Thus if we have a scene \(S\) an image \(I\) and a number of additional parameters \(Y_1 \ldots Y_n\) we can extract the 
posterior probability of \(S\) by integrating out the additional parameters assuming they are independent
\begin{align*}
	P(S,Y_1, \ldots Y_n |I) &= \frac{P(I|S,Y_1,\ldots Y_n)P(S)P(Y_1) \ldots P(Y_n)}{P(I)} \\
	P(S|I) &= \int_{Y_1} \ldots \int_{Y_n}\frac{P(I|S,Y_1,\ldots Y_n)P(S)P(Y_1) \ldots P(Y_n)}{P(I)} dY_n \ldots dY_1
\end{align*}
Or by summing them out in the discrete case.

If we have a scene \(S\) and a number of additional parameters \(Y_1 \ldots Y_n\) that through application of an image generating
function \(G(S,Y_1 \ldots ,Y_n)\) yields an image \(I=G(S,Y_1 \ldots,Y_n)\). We can estimate the scene with the maximum posterior probability 
\(P(S,Y_1,\ldots Y_n|I)\) as the scene and set of parameters that yield the smallest difference between the observed image
and the image calculated with \(G\). Thus, if the scene with the highest posterior likelihood is \(S_{MP}\) we
have 
\begin{equation}
	S_{MP} = \text{argmin}_{S,Y_1, \ldots Y_n}\left(( I-G(S,Y_1, \ldots Y_n) \right)^2 )
	\label{eq:sceneMP}
\end{equation}
We noted earlier that an infinite number of parameters may explain an image equally well and that Freeman proposed a solution
to this that involves discounting a small area around the chosen value of a parameter. Freeman considered shape from shading with
the only additional parameter being the lighting direction. In his case we can set up eq.~\ref{eq:sceneMP} considering a small
area around the chosen lighting direction \(Y\)
\begin{equation}
	S_{MP} = \text{argmin}_{S,Y_0}\left(\int_{Y_0-\Delta Y}^{Y_0+\Delta Y}(I-G(S,Y) )^2dY \right)
	\label{eq:sceneMPF}
\end{equation}

The way this is achieved is by using Pentland's linear shape from shading method that will generate a scene \(S\) from a 
lighting direction \(Y\) and an image \(I\). This is essentially the inverse image generation function \(S=G^{-1}(I,Y)\).
Applying this function in eq.~\ref{eq:sceneMPF} we have the scene with the maximum posterior likelihood as a function of the
lighting direction \(Y\) only.

\begin{equation}
	S_{MP} = \text{argmin}_{Y_0}\left( \int_{Y_0-\Delta Y}^{Y_0+\Delta Y}(I-G(G^{-1}(I,Y_0),Y))^2dY\right)
	\label{eq:sceneMPY}
\end{equation}
In a real implementation that deals with anything but simple toy problems, the integral in the above equation will
need to be replaced with a sum.

\subsection{Linear shape from shading}
Pentland's linear shape from shading assumes that a surface has lambertian reflectance. The fourier transformation of the 
three-dimensional shape \(z\) can then be found from any image \(I\) as follows
\begin{equation*}
	F_z = H^{-1}F_I
\end{equation*}
Where \(H\) is the transfer function creating the fourier transformed image \(F_I\) from the fourier transform of the shape \(F_z\).
\(H\) is given by 
\begin{equation*}
	H = 2\pi f e^{\frac{i \pi}{2}}(k_2\cos(\theta) + k_3\sin(\theta))
\end{equation*}
Where \(k_2\) and \(k_3\) are dependant on the assumed lighting direction that can be specified using the azimuth angle \(\tau\) and
elevation angle \(\sigma\), defined as:
\begin{align*}
	k_2 &= \cos(\tau)\sin\left(\frac{\pi}{2} -\sigma\right) \\
	k_3 &= \sin(\tau)\sin\left(\frac{\pi}{2} -\sigma\right)
\end{align*}
The fourier transform of an image is another "image" with the DC component in the center. Superimposing a polar coordinate system
with the center in the middle of the fourier transform yields the frequency \(f\) as the magnitude and \(\theta\) as the angle.


\section{Implementation and evaluation}
\subsection{Shape from shading}
Consider the image in figure~\ref{fig:bump}. This is commony perceived as a bump even though an infinite number of shapes
can explain the image depending on the lighting conditions.
Pentland's linear shape from shading was implemented in Matlab and applied to the image. The results for three different assumed
lighting azimuth angles and a constant elevation of 5 degrees are seen in figure~\ref{fig:bumpinterps}. 
The resulting shapes are akin to the ones obtained by Freeman, except 
for the bumps near the edge of (a) and (b). I suspect these are caused by missing boundary conditions and Freeman does not
disclose which boundary conditions he applied.

\begin{figure}[h]
	\center
	\includegraphics[height=3cm]{./pictures/newbump.eps}
	\caption{This image is commonly perceived as a bump because of the prior assumption of light coming from the upper left.}
	\label{fig:bump}
\end{figure}

\begin{figure}[h]
	\center
	\begin{subfigure}[b]{0.4\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/bump0deg.eps}
		\caption{}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/bump180deg.eps}
		\caption{}
	\end{subfigure}

	\begin{subfigure}[b]{0.4\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/bump30deg.eps}
		\caption{}
	\end{subfigure}
	\caption{Interpretations of the image in figure~\ref{fig:bump} for an assumed light direction of 180 degrees (a), 0 degrees (b) and
	30 degrees (c). The assumed lighting direction is applied, but the viewpoint is changed.}
	\label{fig:bumpinterps}
\end{figure}

When applied to the images shown in figure~\ref{fig:uplprior} for an assumed light azimuth angle of 90 degrees, meaning from the top down,
and elevation of 5 degrees, the images in figure~\ref{fig:sfsinterp} are created. The images shown are median filtered with a 
kernel of size 21 by 21 samples because the output image becomes very noisy near 90 degrees azimuth. This is caused by the 
fourier components of the image mostly lying along the vertical axis and when the light direction approaches this they become
invisible. The same is true for the results in figure~\ref{fig:bumpinterps} and it may contribute to the unexplained sidelobes.
However, the shape is still clearly discernable and the expected configuration of convex and concave images are easily seen.
Shapes reconstructed for other assumed lighting directions become very complex in this case and are therefore not shown.

\begin{figure}[h]
	\center
	\begin{subfigure}[b]{0.4\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/sfs90deg.eps}
		\caption{}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\center
		\includegraphics[height=3cm]{./pictures/sfs270deg.eps}
		\caption{}
	\end{subfigure}
	\caption{Most likely scenes for the pictures in figure~\ref{fig:uplprior} (a) and (b) respectively.}
	\label{fig:sfsinterp}
\end{figure}

\subsection{Bayesian shape from shading}
The resulting shapes in the previous section were found with a known lighting direction. Freeman proposed a shape from 
shading algorithm that both found and evaluated hypothesis shapes according to eq.~\ref{eq:sceneMPY}. This can be 
implemented by substituting the integral with a sum over adjacent angles and evaluating the expression for a number of starting angles.
The starting angles may be chosen according to any scheme, in my implementation the elevation angle is fixed at 5 degrees above
the image plane and the expression is evaluated for azimuth angles \(Y_0\in [0,5,\ldots 355]\). Thus the light sources are positioned
on a circle 5 degrees above the image plane with an azimuth resolution of 5 degrees. The hypothesis shape generated by the inverse
image function \(G^{-1}\) is then evaluated for a \(\Delta Y\) of 5 degrees with a resolution of 1 degree. As described, eq.~\ref{eq:sceneMPY} 
becomes:
\begin{equation*}
	S_{MP} = \text{argmin}_{Y_0}\left( \sum_{Y=Y_0-5^\circ}^{Y_0+5^\circ}(I-G(G^{-1}(I,Y_0),Y))^2\right) \quad , \quad Y_0 \in [0,5,\ldots,355]
\end{equation*}
When evaluated for the image in figure~\ref{fig:bump} the resulting shape with maximum likelihood is the one shown in figure~\ref{fig:bumpinterps}(a). 
Figure~\ref{fig:uplprior}(a) results in the shape shown in figure~\ref{fig:sfsinterp}(a) and figure~\ref{fig:uplprior}(b) 
results in the shape shown in figure~\ref{fig:sfsinterp}(b).
Thus, for the tested shapes, the approach yields the same shape as that perceived by a human observer.

\section{Conclusion}
The human visual system perceives distinctive shapes when presented with images that have an infinite number of valid 
interpretations. Freeman proposed that the perceived shape is the shape with the highest likelihood of natural occurence~\cite{Freeman1996} and evaluated the likelihood based on the accidentalness of the lighting direction. I have replicated his procedure for 
evaluating hypothesis shapes generated by Pentland's linear shape from shading method and extended it to automatically infer
the most likely lighting direction based on the difference in shape for small changes in the direction of applied light, thereby
discounting the direction of light.

The algorithm was tested on the images in figures~\ref{fig:bump} and~\ref{fig:uplprior} where it found the same shape
as a human observer perceives. This result supports Freeman's proposal that the visual system chooses to perceive a shape
according to the degree of accidentalness, in addition to employing a prior for light sources being located in the upper left~\cite{Mamassian2002}.


\bibliographystyle{IEEEtran}
\bibliography{library}

\end{document}
